# frozen_string_literal: true


def GetProducts
  [
    {code: 1, name: 'Red Scarf', price: 9.25},
    {code: 2, name: 'Silver cufflinks', price: 45.00},
    {code: 3, name: 'Silk Dress', price: 19.95}
  ]
end


class Checkout
  def initialize(rules)
    @rules = rules
    @basket = Array.new
    products.each do |product|
      temp_prod = product
      temp_prod[:count] = 0
      @basket.push(temp_prod)
    end
  end

  def scan(item_code)
    item = @basket.detect { |e| e[:code] == item_code }
    item[:count] += 1
  end

  def total
    count_total
  end

  private 

  def count_total
    tot_price = 0
    item_rule = @rules.detect { |e| e[:type] == 'item_number' }
    @basket.each do |item|
      temp_rule = item_rule[:items].detect { |e| e[:item_code] == item[:code] }
      if !temp_rule.nil? && temp_rule[:item_number] <= item[:count]
        tot_price += item[:count] * temp_rule[:new_price]
      else
        tot_price += item[:count] * item[:price]
      end
    end
    tot_rule = @rules.detect { |e| e[:type] == 'total' }
    if !tot_rule.nil? && tot_price >= tot_rule[:total]
      tot_price *= (1 - tot_rule[:discount].to_f/100)
    end
    tot_price
  end

  def products
    @products = GetProducts()
  end
end


prom_rules = [
  {type: 'total', total: 60, discount: 10 },
  {type: 'item_number', items:
    [ 
    {item_number: 2, item_code: 1, new_price: 8.5 }
    ]
  }
]

c1 = Checkout.new(prom_rules)
c1.scan(1)
c1.scan(2)
c1.scan(3)
puts 'Basket: 001, 002, 003' 
puts 'Total price: £' + (c1.total.round(2)).to_s
puts ''

c2 = Checkout.new(prom_rules)
c2.scan(1)
c2.scan(3)
c2.scan(1)
puts 'Basket: 001, 003, 001' 
puts 'Total price: £' + (c2.total.round(2)).to_s
puts ''

c3 = Checkout.new(prom_rules)
c3.scan(1)
c3.scan(2)
c3.scan(1)
c3.scan(3)
puts 'Basket: 001, 002, 001, 003' 
puts 'Total price: £' + (c3.total.round(2)).to_s